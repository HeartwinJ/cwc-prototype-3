using UnityEngine;

public class BarrierController : MonoBehaviour {
    public float speed;
    public float leftBound;

    private PlayerController playerControllerScript;

    private void Start() {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void Update() {
        if (!playerControllerScript.gameOver) {
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
        if(gameObject.transform.position.x < leftBound) {
            Destroy(gameObject);
        }
    }
}
