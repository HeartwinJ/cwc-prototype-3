using UnityEngine;

public class MoveBackground : MonoBehaviour {
    public float speed;

    private PlayerController playerControllerScript;

    private void Start() {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void Update() {
        if (!playerControllerScript.gameOver) {
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
    }
}
