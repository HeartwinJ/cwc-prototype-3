using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {
    public GameObject barrierPrefab;
    public Vector3 spawnPos;
    public float startTime;
    public float delay;

    private PlayerController playerControllerScript;

    void Start() {
        InvokeRepeating("SpawnBarriers", startTime, delay);
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void SpawnBarriers() {
        if (!playerControllerScript.gameOver) {
            Instantiate(barrierPrefab, spawnPos, barrierPrefab.transform.rotation);
        }
    }
}
